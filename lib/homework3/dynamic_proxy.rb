# frozen_string_literal: true

module Homework3
  class DynamicProxy
    def initialize(attributes)
      @attributes = attributes.map { |key, value| [key.to_sym, value] }.to_h
    end

    def method_missing(key, *args)
      if respond_to_missing?(key, nil)
        @attributes[key]
      else
        super
      end
    end

    # Locally redefined the respond_to_missing?

    def respond_to_missing?(key, *)
      if @attributes.key?(key) && !@attributes[key].nil? # Not nil
        true
      else
        super # main method respond_to_missing?
      end
    end
  end
end
