# frozen_string_literal: true

class String
  def to_proc
    proc do |obj, *args|
      obj.send self, *args
    end
  end
end

module Homework3
  module CoreExtensions
    module EnumerableFriendlyString
      # put your code here
    end
  end
end
